from django.shortcuts import render

from sendingemail.models import UserEmail

def index(request):

    user_info = UserEmail.objects.all().order_by('name_person')

    return render(request, 'sendingemail/index.html',{
                  "user_info": user_info,
                  })
