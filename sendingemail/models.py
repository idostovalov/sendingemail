from django.db import models

class UserEmail(models.Model):
    name_person = models.TextField(max_length=200)
    email_person = models.EmailField(max_length=200, blank=False)

    def __str__(self):
        return self.email_person

    def __str__(self):
        return self.name_person

