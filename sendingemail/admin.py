from django.contrib import admin

# Register your models here.
from sendingemail.models import UserEmail

class PollAdmin(admin.ModelAdmin):
    fields = ['email_person', 'name_person']

admin.site.register(UserEmail, PollAdmin)