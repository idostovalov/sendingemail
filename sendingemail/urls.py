__author__ = 'igor'
from django.conf.urls import patterns, url

from sendingemail import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index')
)